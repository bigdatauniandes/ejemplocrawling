package co.edu.uniandes.crawling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

/**
 * Objeto para acceso al repositorio de objetos
 *
 */
@Repository
public class EventoDAO extends AbstractDAO<Evento> {

	public static final String PARAMETRO_LUGAR = "lugar";

	public static final String PARAMETRO_FECHA = "fecha";

	public static final String PARAMETRO_DEPARTAMENTO = "departamento";

	public static final String PARAMETRO_FACULTAD = "facultad";

	/**
	 * Constructor
	 */
	public EventoDAO() {
		super();
		cargarEventos();
	}
	
	/**
	 * Agrega un evento del listado
	 * 
	 * @param nuevoEvento
	 */
	public void agregarEvento(Evento nuevoEvento) {
		this.agregar(nuevoEvento);
	}

	/**
	 * Borra un evento del listado
	 * 
	 * @param eventoAEliminar
	 */
	public void borrarEvento(Evento eventoAEliminar) {
		this.eliminar(eventoAEliminar);
	}

	/**
	 * Devuelve todos los eventos
	 * 
	 * @return
	 */
	public Set<Evento> obtenerTodosEventos() {
		return this.obtenerTodos();
	}

	/**
	 * Permite cargar los eventos
	 * 
	 * @param eventos
	 */
	private void cargarEventos() {
		
		Set<Evento> eventos = new TreeSet<>();
		for(int i = 1; i <= 20; i++){
			eventos.add(new Evento(Integer.toString(i), "Evento " + i, 
					new Date().getTime(), "Lugar " + i, 
					"Responsable " + i, "responsable"+ i+"@email.com", 
					"http://uniandes.edu.co/evento/" + i, "Descripcion " + i, 
					Arrays.asList("palabra", "clave", "otra"), "Facultad " + i, 
					"Departamento " + i));
		}		
		this.cargarDatosEnListado(eventos);
	}

	/**
	 * Permite obtener eventos por:
	 * <ul>
	 * 	<li>Lugar del evento</li>
	 * 	<li>Fecha del evento</li>
	 * 	<li>Departamento que organiza el evento</li>
	 * 	<li>Facultad que organiza el evento</li>
	 * </ul>
	 * @param criterios
	 * @return
	 */
	public Set<Evento> obtenerEventosPorVariosCriterios(Map<String, Object> criterios){
		
		List<Predicate<Evento>> filtros = new ArrayList<>();
		
		for(Map.Entry<String, Object> criterio : criterios.entrySet()){
			
			switch(criterio.getKey()){	
			
				case PARAMETRO_LUGAR:
					filtros.add(filtrarEventoPorLugar((String)criterio.getValue()));
					break;
				case PARAMETRO_DEPARTAMENTO:
					filtros.add(filtrarEventoPorDepartamento((String)criterio.getValue()));
					 break;
				case PARAMETRO_FACULTAD:
					filtros.add(filtrarEventoPorFacultad((String)criterio.getValue()));
					break;
				case PARAMETRO_FECHA:
					filtros.add(filtrarEventoPorFechaYHora((long)criterio.getValue()));
					break;
				default:
					break;
			}
		}
		
		if(filtros.isEmpty()){
			return this.obtenerTodos();
		}
		
		else{
			Predicate<Evento> filtrosCompuestos = filtros.stream().reduce(predicate -> true, Predicate::and);
			return this.obtenerTodos().stream().filter(filtrosCompuestos).collect(Collectors.toSet());
		}
	}

	/**
	 * Permite filtrar eventos por lugar
	 * 
	 * @param lugarEvento
	 * @return
	 */
	private static Predicate<Evento> filtrarEventoPorLugar(String lugarEvento) {
		return (evento) -> evento.getLugarEvento()
				.equalsIgnoreCase(lugarEvento);
	}

	/***
	 * Permite filtrar eventos por Fecha y Hora (milisegundos epoca unix)
	 * 
	 * @param fechaYHoraEvento
	 * @return
	 */
	private static Predicate<Evento> filtrarEventoPorFechaYHora(
			long fechaYHoraEvento) {
		return evento -> evento.getFechaEvento() == fechaYHoraEvento;
	}

	/**
	 * Permite filtrar eventos por facultad
	 * 
	 * @param facultadEvento
	 * @return
	 */
	private static Predicate<Evento> filtrarEventoPorFacultad(
			String facultadEvento) {
		return evento -> evento.getFacultadEvento().equalsIgnoreCase(
				facultadEvento);
	}

	/**
	 * Permite filtrar eventos por departamento
	 * 
	 * @param departamentoEvento
	 * @return
	 */
	private static Predicate<Evento> filtrarEventoPorDepartamento(
			String departamentoEvento) {
		return evento -> evento.getDepartamentoEvento().equalsIgnoreCase(
				departamentoEvento);
	}
}