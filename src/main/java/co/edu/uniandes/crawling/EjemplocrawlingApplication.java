package co.edu.uniandes.crawling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

/**
 * 
 * Clase principal de la aplicacion
 *
 */
@SpringBootApplication
public class EjemplocrawlingApplication {

	/**
	 * Almacena la informacion de los sitios ya visitados
	 */
	private static final String RUTA_CRAWLING = "/home/estudiante/crawling";

	/**
	 * Nivel de profundidad en la que se indexa la estructura del sitio<br/>
	 * -1 es ilimitado
	 */
	private static final int PROFUNDIDAD_CRAWLING = 5;

	/**
	 * Cantidad maxima de paginas a indexar<br/>
	 * -1 es ilimitado
	 */
	private static final int NIVELES_CRAWLING = -1;

	/**
	 * Pagina o semilla inicial para hacer web crawling
	 */
	private static final String PAGINA_INICIO_UNIANDES = "http://uniandes.edu.co/";

	/**
	 * Tiempo de espera para cada peticion, en milisegundos
	 */
	private static final int INTERVALO_PETICIONES = 200;

	public static void main(String[] args) {

		SpringApplication.run(EjemplocrawlingApplication.class, args);

		try {

			// Configurar el crawler
			CrawlConfig configuracionCrawler = configurarCrawler();
			CrawlController controladorCrawler = configurarControladorCrawling(configuracionCrawler);

			// Indicarle la pagina desde donde inicia
			controladorCrawler.addSeed(PAGINA_INICIO_UNIANDES);

			// Iniciar crawler
			controladorCrawler.start(Crawler.class, 1);

			// Cargar Eventos ya descubiertos
		} catch (Exception e) {
			System.err.println("Ocurrio un error al iniciar el crawler: " + e.getMessage());
		}
	}

	/**
	 * Se configura:
	 * <ul>
	 * <li>Ruta para guardar resultados</li>
	 * <li>Profundidad crawling</li>
	 * <li>Niveles crawling</li>
	 * <li>Tiempo a esperar entre peticiones</li>
	 * </ul>
	 * 
	 * @return
	 */
	private static CrawlConfig configurarCrawler() {

		CrawlConfig configuracionCrawler = new CrawlConfig();
		configuracionCrawler.setCrawlStorageFolder(RUTA_CRAWLING);
		configuracionCrawler.setMaxDepthOfCrawling(PROFUNDIDAD_CRAWLING);
		configuracionCrawler.setMaxPagesToFetch(NIVELES_CRAWLING);
		configuracionCrawler.setResumableCrawling(true);
		configuracionCrawler.setPolitenessDelay(INTERVALO_PETICIONES);
		return configuracionCrawler;
	}

	/**
	 * Inicia la actividad de crawling teniendo en cuenta Los archivos
	 * robots.txt y una configuracion previamente definida
	 * 
	 * @param configuracionCrawler
	 *            Configuracion de crawling
	 * @return
	 * @throws Exception
	 */
	private static CrawlController configurarControladorCrawling(CrawlConfig configuracionCrawler) throws Exception {

		PageFetcher lectorPagina = new PageFetcher(configuracionCrawler);
		RobotstxtConfig configuracionRobotsTxt = new RobotstxtConfig();
		RobotstxtServer robotstxtServer = new RobotstxtServer(configuracionRobotsTxt, lectorPagina);
		return new CrawlController(configuracionCrawler, lectorPagina, robotstxtServer);
	}
}
