package co.edu.uniandes.crawling;

import java.util.List;

/**
 * 
 * Representa un evento de la Universidad de los Andres
 *
 */
public class Evento implements Comparable<Evento> {

	private String idEvento;

	private String nombreEvento;

	private long fechaEvento;

	private String lugarEvento;

	private String nombreContactoEvento;

	private String correoContatoEvento;

	private String enlaceEvento;

	private String descripcionEvento;

	private List<String> palabrasClavesEvento;

	private String facultadEvento;

	private String departamentoEvento;

	/**
	 * 
	 */
	public Evento() {
	}

	/**
	 * 
	 * @param idEvento
	 * @param nombreEvento
	 * @param fechaEvento
	 * @param lugarEvento
	 * @param nombreContactoEvento
	 * @param correoContatoEvento
	 * @param enlaceEvento
	 * @param descripcionEvento
	 * @param palabrasClavesEvento
	 * @param facultadEvento
	 * @param departamentoEvento
	 */
	public Evento(String idEvento, String nombreEvento, long fechaEvento,
			String lugarEvento, String nombreContactoEvento,
			String correoContatoEvento, String enlaceEvento,
			String descripcionEvento, List<String> palabrasClavesEvento,
			String facultadEvento, String departamentoEvento) {
		super();
		this.idEvento = idEvento;
		this.nombreEvento = nombreEvento;
		this.fechaEvento = fechaEvento;
		this.lugarEvento = lugarEvento;
		this.nombreContactoEvento = nombreContactoEvento;
		this.correoContatoEvento = correoContatoEvento;
		this.enlaceEvento = enlaceEvento;
		this.descripcionEvento = descripcionEvento;
		this.palabrasClavesEvento = palabrasClavesEvento;
		this.facultadEvento = facultadEvento;
		this.departamentoEvento = departamentoEvento;
	}

	/**
	 * @return the idEvento
	 */
	public String getIdEvento() {
		return idEvento;
	}

	/**
	 * @param idEvento
	 *            the idEvento to set
	 */
	public void setIdEvento(String idEvento) {
		this.idEvento = idEvento;
	}

	/**
	 * @return the nombreEvento
	 */
	public String getNombreEvento() {
		return nombreEvento;
	}

	/**
	 * @param nombreEvento
	 *            the nombreEvento to set
	 */
	public void setNombreEvento(String nombreEvento) {
		this.nombreEvento = nombreEvento;
	}

	/**
	 * @return the fechaEvento
	 */
	public long getFechaEvento() {
		return fechaEvento;
	}

	/**
	 * @param fechaEvento
	 *            the fechaEvento to set
	 */
	public void setFechaEvento(long fechaEvento) {
		this.fechaEvento = fechaEvento;
	}

	/**
	 * @return the lugarEvento
	 */
	public String getLugarEvento() {
		return lugarEvento;
	}

	/**
	 * @param lugarEvento
	 *            the lugarEvento to set
	 */
	public void setLugarEvento(String lugarEvento) {
		this.lugarEvento = lugarEvento;
	}

	/**
	 * @return the nombreContactoEvento
	 */
	public String getNombreContactoEvento() {
		return nombreContactoEvento;
	}

	/**
	 * @param nombreContactoEvento
	 *            the nombreContactoEvento to set
	 */
	public void setNombreContactoEvento(String nombreContactoEvento) {
		this.nombreContactoEvento = nombreContactoEvento;
	}

	/**
	 * @return the correoContatoEvento
	 */
	public String getCorreoContatoEvento() {
		return correoContatoEvento;
	}

	/**
	 * @param correoContatoEvento
	 *            the correoContatoEvento to set
	 */
	public void setCorreoContatoEvento(String correoContatoEvento) {
		this.correoContatoEvento = correoContatoEvento;
	}

	/**
	 * @return the enlaceEvento
	 */
	public String getEnlaceEvento() {
		return enlaceEvento;
	}

	/**
	 * @param enlaceEvento
	 *            the enlaceEvento to set
	 */
	public void setEnlaceEvento(String enlaceEvento) {
		this.enlaceEvento = enlaceEvento;
	}

	/**
	 * @return the descripcionEvento
	 */
	public String getDescripcionEvento() {
		return descripcionEvento;
	}

	/**
	 * @param descripcionEvento
	 *            the descripcionEvento to set
	 */
	public void setDescripcionEvento(String descripcionEvento) {
		this.descripcionEvento = descripcionEvento;
	}

	/**
	 * @return the palabrasClavesEvento
	 */
	public List<String> getPalabrasClavesEvento() {
		return palabrasClavesEvento;
	}

	/**
	 * @param palabrasClavesEvento
	 *            the palabrasClavesEvento to set
	 */
	public void setPalabrasClavesEvento(List<String> palabrasClavesEvento) {
		this.palabrasClavesEvento = palabrasClavesEvento;
	}

	/**
	 * @return the facultadEvento
	 */
	public String getFacultadEvento() {
		return facultadEvento;
	}

	/**
	 * @param facultadEvento
	 *            the facultadEvento to set
	 */
	public void setFacultadEvento(String facultadEvento) {
		this.facultadEvento = facultadEvento;
	}

	/**
	 * @return the departamentoEvento
	 */
	public String getDepartamentoEvento() {
		return departamentoEvento;
	}

	/**
	 * @param departamentoEvento
	 *            the departamentoEvento to set
	 */
	public void setDepartamentoEvento(String departamentoEvento) {
		this.departamentoEvento = departamentoEvento;
	}

	@Override
	public int compareTo(Evento otroEvento) {

		if (this.fechaEvento > otroEvento.getFechaEvento()) {
			return 1;
		}

		else if (this.fechaEvento < otroEvento.getFechaEvento()) {
			return -1;
		}

		else {
			return this.idEvento.compareTo(otroEvento.getIdEvento());
		}
	}

	@Override
	public boolean equals(Object otroEvento){
		
		if(otroEvento instanceof Evento){
			
			if(this != null && otroEvento != null){
				Evento evento = (Evento) otroEvento;
				return this.hashCode() == evento.hashCode();				
			}
			else if(this == null && otroEvento == null){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode(){
		int hash = 7;
		hash += (5 * idEvento.hashCode());
		hash += (5 * nombreEvento.hashCode());
		hash += (5 * lugarEvento.hashCode());     
		hash += (5 * nombreContactoEvento.hashCode());
        hash += (5 * correoContatoEvento.hashCode());         
        hash += (5 * enlaceEvento.hashCode());                
		hash += (5 * descripcionEvento.hashCode());   
		hash += (5 * fechaEvento);
		hash += (5 * facultadEvento.hashCode());
		hash += (5 * departamentoEvento.hashCode());
		for(String palabra : palabrasClavesEvento){
			hash += (5 * palabra.hashCode());
		}
		return hash;
	}
}
