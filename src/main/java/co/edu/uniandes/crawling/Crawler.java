package co.edu.uniandes.crawling;

import java.util.Set;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

public class Crawler extends WebCrawler {

	/**
	 * Expresion regular para descargar archivos: <li>
	 * <ul>
	 * Estilos css
	 * </ul>
	 * <ul>
	 * Scripts js
	 * </ul>
	 * <ul>
	 * Imagenes gif, jpg, png
	 * </ul>
	 * <ul>
	 * Audio mp3
	 * </ul>
	 * <ul>
	 * Comprimidos zip, gz
	 * </ul>
	 * </li>
	 * 
	 * . => Cualquier caracter al inicio * => Cero o n veces repetido (..(..))
	 * => tener en cuenta el subgrupo $ => final del texto \\. => caracter punto
	 * (.), cualquier coincidencia literal con punto abc|def => cualquier
	 * coincidencia con abc o def
	 */
	private static final Pattern FILTERS = Pattern
			.compile(".*(\\.(css|js|gif|jpg|png|mp3|zip|gz))$");

	private static final String DOMINIO_UNIANDES = "uniandes.edu.co";
	
	private static final Logger LOGGER = Logger.getLogger(Crawler.class);

	/**
	 * Determina si debe visitar la pagina o no
	 */
	@Override
	public boolean shouldVisit(Page referringPage, WebURL url) {
		
		String enlaceSitio = url.getURL().toLowerCase();
		LOGGER.info("Verificando sitio " + enlaceSitio);
		return !FILTERS.matcher(enlaceSitio).matches()
				&& (enlaceSitio.contains(DOMINIO_UNIANDES));
	}

	/**
	 * Proceso a realizar al visitar cada página
	 */
	@Override
	public void visit(Page page) {
		String enlaceSitio = page.getWebURL().getURL();
		LOGGER.info("Visitando sitio " + enlaceSitio);

		if (page.getParseData() instanceof HtmlParseData) {
			HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
			String text = htmlParseData.getText();
			String html = htmlParseData.getHtml();
			Set<WebURL> links = htmlParseData.getOutgoingUrls();
			// TODO Extraccion de eventos
		}
	}

}
