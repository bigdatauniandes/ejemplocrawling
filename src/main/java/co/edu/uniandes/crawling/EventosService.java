package co.edu.uniandes.crawling;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EventosService {
	
	@Autowired
	private EventoDAO eventosRepositorio;
	
	@RequestMapping(path="/eventos", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON)
	public Set<Evento> obtenerEventos(){
		return eventosRepositorio.obtenerTodosEventos();
	}
	
	@RequestMapping(path="/eventos/filtrar", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON)
	public Set<Evento> filtrarEventos(
			@RequestParam(required=false) Map<String, String> queryParameters){
		
		Map<String,Object> criterios = new HashMap<>();
		queryParameters.forEach((key,value) -> {
			switch(key){
				
				case EventoDAO.PARAMETRO_LUGAR:
				case EventoDAO.PARAMETRO_DEPARTAMENTO:
				case EventoDAO.PARAMETRO_FACULTAD:
					criterios.put(key,value);
					break;
				case EventoDAO.PARAMETRO_FECHA:
					criterios.put(key, Long.parseLong(value));
					break;
			}
		});
		return eventosRepositorio.obtenerEventosPorVariosCriterios(criterios);
	}
}