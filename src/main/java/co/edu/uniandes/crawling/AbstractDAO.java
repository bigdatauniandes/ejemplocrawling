package co.edu.uniandes.crawling;

import java.util.Set;
import java.util.TreeSet;

/**
 * 
 * Clase abstracta para implementar repositorios de datos
 *
 * @param <T>
 */
public class AbstractDAO<T> {
	
	private Set<T> listado;
	
	/**
	 * Constructor
	 */
	protected AbstractDAO(){
		listado = new TreeSet<>();
	}
	
	/**
	 * Permite agregar datos al repositorio o listado
	 * @param nuevoListado
	 */
	protected void cargarDatosEnListado(Set<T> nuevoListado){
		listado.addAll(nuevoListado);
	}
	
	/**
	 * Retorna todos los elementos del listado
	 * @return
	 */
	protected Set<T> obtenerTodos(){
		return listado;
	}

	/**
	 * Agrega un nuevo elemento al repositorio o listado
	 * @param nuevo
	 */
	protected void agregar(T nuevo){
		listado.add(nuevo);
	}
	
	protected void eliminar(T elementoAEliminar){
		listado.remove(elementoAEliminar);
	}
	
}
